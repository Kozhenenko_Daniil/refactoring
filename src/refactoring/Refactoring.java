package refactoring;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс для нахождения числа, заданного пользователем, в массиве целых чисел(заданных рандомно)
 *
 * @author Kozhenenko D.D
 */
public class Refactoring {
    private static Scanner scanner = new Scanner(System.in);

    private static final int MIN_NUMBER_IN_ARRAY = -25;
    private static final int RANGE_IN_ARRAY = 51;

    public static void main(String[] args) {

        int arrayLength = input("Введите желаемую длинну массива:");
        int[] array = new int[checkLength(arrayLength)];
        randomArray(array);
        System.out.println(Arrays.toString(array));
        int index = input("Введите значение элемента, индекс которого хотите найти:");
        int result = searchNumber(array, index);
        if (result == -1) {
            System.out.println("Искомого значения нет в списке ");
        } else {
            System.out.println("Индекс искомого элемента в последовательности: " + result);
        }
    }

    /**
     * Возвращает целое число,введенное с консоли, в ответ на сообщение message
     *
     * @param message сообщение пользователю
     *
     * @return целое число
     */
    private static int input(String message) {
        System.out.println(message);
        return scanner.nextInt();
    }

    /**
     * Проверяет полученное число: оно меньше 0,предлагает ввести положительное значение
     *
     * @param arrayLength длинна массива
     *
     * @return длинна массива
     */
    private static int checkLength(int arrayLength) {
        while (0 > arrayLength) {
            System.out.println("Введите положительное число: ");
            arrayLength = scanner.nextInt();
        }
        return arrayLength;
    }

    /**
     * Метод заполнения массива рандомными числами в диапазоне от  MIN_NUMBER_IN_ARRAY до RANGE_IN_ARRAY
     *
     * @param array массив
     */
    private static void randomArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN_NUMBER_IN_ARRAY + (int) (Math.random() * RANGE_IN_ARRAY);
        }
    }

    /**
     * Метод поиска необходимого пользователю индекса числа в массиве
     *
     * @param array массив
     *
     * @return индекс искомого элемента или -1 если такого элемента нет в массиве
     */
    private static int searchNumber(int[] array, int index) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == index) {
                return i;
            }
        }
        return -1;
    }
}